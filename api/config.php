<?php
    session_start();
	$serverHost = "localhost";
	$serverUsername = "root";
	$serverPassword = "";
	$serverName = "salary";

	$con = new mysqli($serverHost, $serverUsername, $serverPassword, $serverName);

	if($con -> connect_errno) {
		echo "Failed to connect to MySql: ".$con -> connect_error;
		exit();
	}

    $daily_limit = 200;
	$salary_date = array (
		"date_1" => 15,
		"date_2" => 30
	);
	
	date_default_timezone_set('Asia/Manila');
?>