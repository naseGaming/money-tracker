<?php
    require_once("config.php");

    if(isset($_GET["getCategoryTypes"])) {
        $count = 0;
        $data_array = array();
		$sql = "SELECT * FROM `category_type`";
		$result = $con -> query($sql);

        while($row = $result -> fetch_assoc()) {
            $data_array[$count] = $row;
            $count++;
        }

        echo json_encode($data_array);
    }

    if(isset($_GET["getCategories"])) {
        $count = 0;
        $data_array = array();
        $categpory_type = $_GET["getCategories"];
		$sql = "SELECT * FROM `categories` where `type` = '$categpory_type'";
		$result = $con -> query($sql);

        while($row = $result -> fetch_assoc()) {
            $data_array[$count] = $row;
            $count++;
        }

        echo json_encode($data_array);
    }

?>