<?php
    require_once("config.php");

    if(isset($_POST["addIncome"])) {
        $date = date("Y-m-d");
        $data = $_POST["addIncome"];

        $description = $data["description"];
        $amount = $data["amount"];
        $category = $data["category"];
        $category_type = $data["category_type"];

        $user = getUser();

        $type = getCategoryTypeDesc($con, $category_type);

		$sql = mysqli_query($con,"INSERT INTO `income` (`description`, `amount`, `category`, `category_type`, `user_id`, `date`) VALUES ('$description', '$amount', '$category', '$category_type', '$user', '$date')");

        if($sql) {
            echo json_encode(Array (
                "type" => "success",
                "data" => $type." added!"
            ));
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "data" => $type." failed to add!"
            ));
        }
    }

    if(isset($_GET["getDate"])) {
		$sql = "SELECT * FROM `log_date`";
		$result = $con -> query($sql);
        $flag = false;
        $date = "";
		while($row = $result -> fetch_assoc()) {
            $flag = true;
            $date = $row["date"];
        }

        if($flag) {
            $new_date = date("Y-m-d");
            if($date != $new_date) {

                $sql = mysqli_query($con,"INSERT INTO `log_date` (`date`) VALUES ('$new_date')");
    
                if($sql) {
                    echo json_encode(Array (
                        "type" => "success",
                        "data" => $date
                    ));
                }
                else {
                    echo json_encode(Array (
                        "type" => "error",
                        "data" => "Internal Error"
                    ));
                }
            }
            else {
                echo json_encode(Array (
                    "type" => "success",
                    "data" => $date
                ));
            }
        }
        else {
            $date = date("Y-m-d");

            $sql = mysqli_query($con,"INSERT INTO `log_date` (`date`) VALUES ('$date')");

            if($sql) {
                echo json_encode(Array (
                    "type" => "success",
                    "data" => $date
                ));
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "data" => "Internal Error"
                ));
            }
        }
    }

    if(isset($_GET["getData"])) {
        if(getUser() === false) {
            echo json_encode(Array (
                "type" => "error",
                "data" => "No User"
            ));
            return;
        }
        $log_date = date("Y-m-d");
        $month = date("m");
        $date = date("Y-m-d", strtotime("-1 day", strtotime($log_date)));
        $yesterday_expense = 0;
        $total_expense = 0;
        $total_money = 0;
        $monthly_saved = 0;
        $monthly_expenses = 0;
        $total_today = 0;
        $count = 0;
        
		$sql = "SELECT * FROM `log_date` where `date` = '$date'";
		$result = $con -> query($sql);
        $row = $result -> fetch_assoc();

        if(isset($row["date"])) {
            $sql_expense = "SELECT * FROM `income` where `date` = '$date' and `category_type` = 2";
            $result_expense = $con -> query($sql_expense);
            while($rows = $result_expense -> fetch_assoc()) {
                if($rows["category"] != 4) {
                    $yesterday_expense += $rows["amount"];
                }
                $count++;
            }
        }

        $sql_expense = "SELECT * FROM `income` where `date` = '$log_date' and `category_type` = 2";
        $result_expense = $con -> query($sql_expense);
        while($rows = $result_expense -> fetch_assoc()) {
            if($rows["category"] != 4) {
                $total_today += $rows["amount"];
            }
            $count++;
        }

        $sql = "SELECT * FROM `income`";
        $result = $con -> query($sql);
        while($row = $result -> fetch_assoc()) {
            if($row["category_type"] == 1) {
                $total_money += $row["amount"];
            }
            if($row["category_type"] == 2) {
                $total_money -= $row["amount"];
            }
            if($row["category_type"] == 3) {
                if($row["category"] == 9) {
                    $total_money -= $row["amount"];
                }
                else {
                    $monthly_saved += $row["amount"];
                }
            }
            if($row["category_type"] == 4) {
                $total_money -= $row["amount"];
            }
        }

        $day_counter = 0;
        $sql = "SELECT * FROM `log_date`";
        $result = $con -> query($sql);
        while($row = $result -> fetch_assoc()) {
            $day_counter++;
        }

        $sql_expense = "SELECT * FROM `income` where `category_type` = 2";
        $result_expense = $con -> query($sql_expense);
        while($rows = $result_expense -> fetch_assoc()) {
            if($rows["category"] != 4) {
                $total_expense += $rows["amount"];
            }
        }

        $average_daily = $total_expense / $day_counter;

        $daily_left = $total_money / $average_daily;

        $sql = "SELECT * FROM `income` where MONTH(`date`) = '$month'";
        $result = $con -> query($sql);
        while($row = $result -> fetch_assoc()) {
            if($row["category_type"] == 1) {
                $monthly_saved += $row["amount"];
            }
            if($row["category_type"] == 2) {
                $monthly_saved -= $row["amount"];
            }
            if($row["category_type"] == 3) {
                if($row["category"] == 9) {
                    $monthly_saved -= $row["amount"];
                }
                else {
                    $monthly_saved += $row["amount"];
                }
            }
            if($row["category_type"] == 4) {
                $monthly_saved -= $row["amount"];
            }
        }

        $sql = "SELECT * FROM `income` where MONTH(`date`) = '$month'";
        $result = $con -> query($sql);
        while($row = $result -> fetch_assoc()) {
            if($row["category_type"] == 2) {
                $monthly_expenses += $row["amount"];
            }
            if($row["category_type"] == 3) {
                if($row["category"] == 9) {
                    $monthly_expenses += $row["amount"];
                }
            }
            if($row["category_type"] == 4) {
                $monthly_expenses += $row["amount"];
            }
        }

        $data = array(
            "type" => "success",
            "total_money" => $total_money,
            "yesterday_total_expense" => $yesterday_expense,
            "today_total_expense" => $total_today,
            "average_daily_expense" => $average_daily,
            "daily_expense_limit" => $daily_limit,
            "predicted" => $daily_left,
            "monthly_saved" => $monthly_saved,
            "monthly_expenses" => $monthly_expenses
        );

        echo json_encode($data);
    }
    
    if(isset($_GET["getHistory"])) {
        $count = 0;
        $data_array = array();
        $sql = "SELECT * FROM `income` order by `id` desc";
        $result = $con -> query($sql);

        while($row = $result -> fetch_assoc()) {
            $data_array[$count] = $row;
            $count++;
        }

        echo json_encode($data_array);
    }

    function getUser() {
        if(isset($_SESSION["user"])) {
            return $_SESSION["user"];
        }
        else {
            return false;
        }
    }

    function getCategoryTypeDesc($con, $id) {
		$sql = "SELECT `description` FROM `category_type` where `id` = '$id'";
		$result = $con -> query($sql);

        while($row = $result -> fetch_assoc()) {
            return $row["description"];
        }
    }
?>