<?php
    require_once("config.php");

    if(isset($_POST["login"])) {
        $data = $_POST["login"];

		$sql = "SELECT * FROM `accounts` where `username` = '".$data["user"]."'";
		$result = $con -> query($sql);
        $flag = false;

		while($row = $result -> fetch_assoc()) {
            $flag = true;
            if(password_verify($data["pass"], $row["password"])) {
                $_SESSION["user"] = $row["id"];
                echo json_encode(Array (
                    "type" => "success",
                    "data" => "login successful!"
                ));
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "data" => "Incorrect password!"
                ));
            }
        }

        if(!$flag) {
            echo json_encode(Array (
                "type" => "error",
                "data" => "Username does not exist!"
            ));
        }
    }

    if(isset($_GET["logout"])) {
        session_destroy();

        if(isset($_SESSION["user"])) {
            echo json_encode(Array (
                "type" => "success",
                "data" => "Logged out"
            ));
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "data" => "Logged out"
            ));
        }
    }

?>