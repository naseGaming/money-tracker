# Money Tracker

Money Tracker

## Name
Money Tracker

## Description
This is a simple system that tracks your income/expenses everyday. The limitation is this is not connected into any payment portal or anything so you will have to input them on your own. This is available on http://salary-tracker.epizy.com/?i=1. So for now you will have to input your digits truthfully as it will defeat the purpose if you will just put random numbers on it.

## Contributing
Just pull the public branch if you want to contribute and file for merge requests.

## Authors and acknowledgment
Roland Christian Too Regacho

## Project status
In development

