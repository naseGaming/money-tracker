$(() => {
    //resetLoginData()
    $.when(setNotification("scripts/notification-too"))
    .done(() => {
        getDate()
        getData()
        getCategoryTypes()
        getHistory()
    
        $('form').on('submit', function(e) {
            e.preventDefault()
            let id = this.id
            let data = ""

            if($("#category").val == "") {
                showNotification("Category is required!", error)
            }
            
            data = {
                "description": $("#income_description").val(),
                "amount": $("#income_amount").val(),
                "category_type": $("#category_type").val(),
                "category": $("#category").val()
            }
            addIncome(data)
        })

        $("#logout").click(() => {
            logout()
        })
    })
})

function getDate() {
    $.ajax({
        type: "GET",
        url: "./api/income.php",
        data: jQuery.param({ getDate:"" }),
        processData: false
    })
    .done(response => {
        let data = JSON.parse(response)

        if(data.type == "success") {
            $("#date").html(data.data)
        }
        else {
            showNotification(data.data, data.type)
        }
    })
}

function getCategoryTypes() {
    $.ajax({
        type: "GET",
        url: "./api/categories.php",
        data: jQuery.param({ getCategoryTypes:"" }),
        processData: false
    })
    .done(response => {
        let data = JSON.parse(response)
        let render = '<option value = "" >--No Category Type selected!--</option>'

        for(let items in data) {
            render += `<option value = "${data[items].id}">${data[items].description}</option>`
        }

        $("#category_type").html(render)
    })
}

function getCategories(app) {
    let id = app.value

    $.ajax({
        type: "GET",
        url: "./api/categories.php",
        data: jQuery.param({ getCategories:id }),
        processData: false
    })
    .done(response => {
        let data = JSON.parse(response)
        let render = `<option value = "" >--No Category selected!--</option>`

        for(let items in data) {
            render += `<option value = "${data[items].id}">${data[items].description}</option>`
        }

        $("#category").html(render)
    })
}

function addIncome(data) {
    $.ajax({
        type: "POST",
        url: "./api/income.php",
        data: jQuery.param({ addIncome: data }),
        processData: false
    })
    .done(response => {
        let data = JSON.parse(response)

        showNotification(data.data, data.type)

        if(data.type == "success") {
            clearInputs()
        }

        getData()
        getHistory()
    })
}

function getData() {
    $.ajax({
        type: "GET",
        url: "./api/income.php",
        data: jQuery.param({ getData: "" }),
        processData: false
    })
    .done(response => {
        let data = JSON.parse(response)

        if(data.type == "error") {
            window.location.href = "./login.html"
        }
        
        for(let key in data) {
            let value = data[key]

            if(value > 0) {
                value = value.toFixed(2)
            }

            if(key == "yesterday_total_expense" || key == "today_total_expense" || key == "average_daily_expense") {
                if(value > 200) {
                    $("#" + key).attr("style", "color: red")
                }
                else {
                    $("#" + key).attr("style", "color: green")
                }
            }
            if(key == "total_money") {
                if(value > 500) {
                    $("#" + key).attr("style", "color: green")
                }
                else {
                    $("#" + key).attr("style", "color: red")
                }
            }
            if(key == "monthly_expenses") {
                if(value > 15000) {
                    $("#" + key).attr("style", "color: red")
                }
                else {
                    $("#" + key).attr("style", "color: green")
                }
            }
            if(key == "predicted") {
                let date = new Date()
                let today = date.getDate()
                if(today < 15) {
                    if(value < 15 - today) {
                        $("#" + key).attr("style", "color: red")
                    }
                    else {
                        $("#" + key).attr("style", "color: green")
                    }
                }
                else {
                    if(value < 30 - today) {
                        $("#" + key).attr("style", "color: red")
                    }
                    else {
                        $("#" + key).attr("style", "color: green")
                    }
                }
            }

            $("#" + key).html(value)
        }
    })
}

function getHistory() {
    $.ajax({
        type: "GET",
        url: "./api/income.php",
        data: jQuery.param({ getHistory: "" }),
        processData: false
    })
    .done(response => {
        let data = JSON.parse(response)
        let render = ""
        
        for(let i in data) {
            render += "<tr>"
            render += `<td>` + data[i].date + `</td>`
            if(data[i].category_type == 1) {
                render += `<td style = "color: green;">+` + data[i].amount + `</td>`
            }
            if(data[i].category_type == 2) {
                render += `<td style = "color: red;">-` + data[i].amount + `</td>`
            }
            if(data[i].category_type == 3) {
                render += `<td style = "color: orange;">-` + data[i].amount + `</td>`
            }
            if(data[i].category_type == 4) {
                render += `<td style = "color: blue;">-` + data[i].amount + `</td>`
            }
            render += `<td>` + data[i].description + `</td>`
            render += "</tr>"
        }


        $("#history").html(render)
    })
}

function logout() {
    $.ajax({
        type: "GET",
        url: "./api/auth.php",
        data: jQuery.param({ logout: "" }),
        processData: false
    })
    .done(response => {
        let data = JSON.parse(response)

        showNotification(data.data, data.type)

        if(data.type == "success") {
            window.location.href = "./login.html"
        }
    })
}

function clearInputs() {
    $("#income_description").val("")
    $("#income_amount").val("")
    $("#expense_description").val("")
    $("#expense_amount").val("")
}